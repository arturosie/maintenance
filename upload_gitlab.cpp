#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

void gitadd (char tabla [100]){ /*Parametro formal*/ //VOID ES UNA FUNCION, la estamos declarando
    /*Definicion de la funcion*/
    char titulo[MAX];
    sprintf(titulo, "git add %s", tabla); //imprime en memoria
    system(titulo);
}

void gitcommit (char tabla [100]){ /*Parametro formal*/ //VOID ES UNA FUNCION, la estamos declarando
    /*Definicion de la funcion*/
    char titulo[MAX];
    sprintf(titulo, "git commit -m \" %s\"", tabla); //imprime en memoria
    system(titulo);
}

//Función punto de entrada
int main(){
    char file [100];
    char commit [100];

    printf("Insert the file path you want to upload to gitlab\n");
    fgets(file, 100, stdin);
    printf("Insert a commit\n");
    fgets(commit, 100, stdin);

    printf("\n");


    gitadd(file);

    gitcommit(commit);


    return EXIT_SUCCESS;
}
